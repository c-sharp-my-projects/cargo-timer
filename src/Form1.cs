﻿using System;
using System.Media;
using System.Windows.Forms;

namespace CargoTimer
{
        public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Application.ApplicationExit += new EventHandler(this.OnApplicationExit);
        }

        private void OnApplicationExit(object sender, EventArgs e)
        {
            allSave();
            CargoTimer.Properties.Settings.Default.min = currentTime.Min;
            CargoTimer.Properties.Settings.Default.sec = currentTime.Sec;
            CargoTimer.Properties.Settings.Default.dateOnClose = DateTime.Now;
            CargoTimer.Properties.Settings.Default.nowStatus = nowStatus;
            CargoTimer.Properties.Settings.Default.Save();
        }
        DateTime dateOnClose;
        string nowStatus;
        string[] statuses = new string[4] {"Стоит в Ост-Терре","Плывет в Ост-Терру","Стоит в Двух Коронах", "Плывет в Две Короны" };
        Time stayTime = new Time("stay"); 
        Time sailingTimeAustera = new Time("sailingToAustera");
        Time sailingTime2c = new Time("sailingTo2c");
        Time currentTime = new Time();

        private void playSimpleSound(string name)
        {
            SoundPlayer simpleSound = new SoundPlayer(@"c:\Windows\Media\"+name+".wav");
            simpleSound.Play();
        }
        private void timerTick()
        {
            if (currentTime.Min == 0 && currentTime.Sec == 0)
            {
                if (nowStatus == statuses[0])
                {
                    nowStatus = statuses[3];
                    currentTime.Min = sailingTime2c.Min; currentTime.Sec = sailingTime2c.Sec;
                }
                else if (nowStatus == statuses[2])
                {
                    nowStatus = statuses[1];
                    currentTime.Min = sailingTimeAustera.Min; currentTime.Sec = sailingTimeAustera.Sec;
                }
                else if (nowStatus == statuses[1])
                {
                    if (checkBox1.Checked && timer1.Enabled) playSimpleSound("tada");
                    nowStatus = statuses[0];
                    currentTime.Min = stayTime.Min; currentTime.Sec = stayTime.Sec;
                }
                else if (nowStatus == statuses[3])
                {
                    if (checkBox1.Checked && timer1.Enabled) playSimpleSound("tada");
                    nowStatus = statuses[2];
                    currentTime.Min = stayTime.Min; currentTime.Sec = stayTime.Sec;
                }
            }

            if(timer1.Enabled) changePicture();
            if (currentTime.Sec == 0) { currentTime.Min--; currentTime.Sec = 60; }
            currentTime.Sec--;
            
            if (checkBox2.Checked && (nowStatus==statuses[0]|| nowStatus == statuses[2]) && (currentTime.TotalSeconds == 300) && timer1.Enabled) playSimpleSound("Alarm01");
        }
        
        private void allSave()
        {            
            CargoTimer.Properties.Settings.Default.onArrive = checkBox1.Checked;
            CargoTimer.Properties.Settings.Default.befor5min = checkBox2.Checked;

            CargoTimer.Properties.Settings.Default.StayMin = stayTime.Min;
            CargoTimer.Properties.Settings.Default.StaySec = stayTime.Sec;
            CargoTimer.Properties.Settings.Default.sailingToAusteraMin = sailingTimeAustera.Min;
            CargoTimer.Properties.Settings.Default.sailingToAusteraSec = sailingTimeAustera.Sec;
            CargoTimer.Properties.Settings.Default.sailingTo2cMin = sailingTime2c.Min;
            CargoTimer.Properties.Settings.Default.sailingTo2cSec = sailingTime2c.Sec;
            CargoTimer.Properties.Settings.Default.Save();
        }
        private void buttonSave()
        {
            timer1.Enabled = false;
            try
            {
                currentTime.Min = Convert.ToUInt32(textBox1.Text); //if (currentTime.Min > 20) { MessageBox.Show("Минут не может быть больше 19 у кораблика", "Ошибка ввода времени", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }
                currentTime.Sec = Convert.ToUInt32(textBox2.Text); //if (currentTime.Sec > 59) { MessageBox.Show("Секунд не может быть больше 59", "Ошибка ввода времени", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }
            }
            catch { MessageBox.Show("Тут должны быть циферки!", "Ошибка ввода времени", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }
            if (comboBox1.SelectedIndex == 0) { nowStatus = statuses[2]; } else if (comboBox1.SelectedIndex == 1) { nowStatus = statuses[0]; } else { MessageBox.Show("Выберите где сейчас стоит кораблик", "Ошибка выбора локации", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }
            timer1.Enabled = true;
            changePicture();
            allSave();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timerTick();
            label1.Text = nowStatus; label2.Text = currentTime.Min.ToString();label3.Text = currentTime.Sec.ToString();
            if(WindowState != FormWindowState.Minimized) label1.Location = new System.Drawing.Point((this.Width-label1.Width)/2,label1.Location.Y);
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                this.ShowInTaskbar = false;
                notifyIcon1.Visible = true;
            }
            if (WindowState == FormWindowState.Normal)
            {
                notifyIcon1.Visible = false;
                this.ShowInTaskbar = true;
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            notifyIcon1.Visible = false;
            this.ShowInTaskbar = true;
            WindowState = FormWindowState.Normal;
        }

        private void pereCount(uint sec)
        {
            uint i = 0;
            while (i < sec)
            {
                timerTick();
                i++;
            }
        }

        private void changePicture()
        {
            if (nowStatus == statuses[3])
            {
                if (currentTime.TotalSeconds < (sailingTime2c.TotalSeconds) / 3)
                    pictureBox1.Image = CargoTimer.Properties.Resources.near2c;
                else
                if (currentTime.TotalSeconds < (sailingTime2c.TotalSeconds) / 3 * 2)
                    pictureBox1.Image = CargoTimer.Properties.Resources.inSeaWest;
                else
                if (currentTime.TotalSeconds < (sailingTime2c.TotalSeconds))
                    pictureBox1.Image = CargoTimer.Properties.Resources.fromAuster;
            }
            else if (nowStatus == statuses[1])
            {
                if (currentTime.TotalSeconds < (sailingTimeAustera.TotalSeconds) / 3)
                    pictureBox1.Image = CargoTimer.Properties.Resources.nearAustera;
                else
                if (currentTime.TotalSeconds < (sailingTimeAustera.TotalSeconds) / 3 * 2)
                    pictureBox1.Image = CargoTimer.Properties.Resources.inSeaEast;
                else
                if (currentTime.TotalSeconds < sailingTimeAustera.TotalSeconds)
                    pictureBox1.Image = CargoTimer.Properties.Resources.from2c;
            }
            else if (nowStatus == statuses[0])
            {
                pictureBox1.Image = CargoTimer.Properties.Resources.inAustera;
            }
            else if (nowStatus == statuses[2])
            {
                pictureBox1.Image = CargoTimer.Properties.Resources.in2c;
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            currentTime.Min = CargoTimer.Properties.Settings.Default.min;
            currentTime.Sec = CargoTimer.Properties.Settings.Default.sec;
            dateOnClose = CargoTimer.Properties.Settings.Default.dateOnClose;
            nowStatus = CargoTimer.Properties.Settings.Default.nowStatus;
            checkBox1.Checked = CargoTimer.Properties.Settings.Default.onArrive;
            checkBox2.Checked = CargoTimer.Properties.Settings.Default.befor5min;

            stayTime.Min = CargoTimer.Properties.Settings.Default.StayMin;
            stayTime.Sec = CargoTimer.Properties.Settings.Default.StaySec;
            sailingTimeAustera.Min = CargoTimer.Properties.Settings.Default.sailingToAusteraMin;
            sailingTimeAustera.Sec = CargoTimer.Properties.Settings.Default.sailingToAusteraSec;
            sailingTime2c.Min = CargoTimer.Properties.Settings.Default.sailingTo2cMin;
            sailingTime2c.Sec = CargoTimer.Properties.Settings.Default.sailingTo2cSec;

            textBox3.Text = stayTime.Min.ToString();
            textBox4.Text = stayTime.Sec.ToString();
            textBox5.Text = sailingTime2c.Min.ToString();
            textBox6.Text = sailingTime2c.Sec.ToString();
            textBox7.Text = sailingTimeAustera.Min.ToString();
            textBox8.Text = sailingTimeAustera.Sec.ToString();


            changePicture();

            System.TimeSpan dateDif = DateTime.Now - dateOnClose;
            DateTime dNow = DateTime.Now;
            if (dateDif.TotalSeconds > 604800) { MessageBox.Show("Вы не запускали программу более недели", "Время и статус не верны!", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
            else
            {
                pereCount(Convert.ToUInt32(dateDif.TotalSeconds));
                while (Convert.ToUInt32((DateTime.Now - dNow).TotalSeconds) > 0)
                {
                    dNow = DateTime.Now;
                    pereCount(Convert.ToUInt32((DateTime.Now - dNow).TotalSeconds));
                }
            }
            timer1.Enabled = true;
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonSave();
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonSave();
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                stayTime.Min = Convert.ToUInt32(textBox3.Text); //if (stayTime.Min > 20) { MessageBox.Show("Минут не может быть больше 19 у кораблика", "Ошибка ввода времени", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }
                stayTime.Sec = Convert.ToUInt32(textBox4.Text); //if (stayTime.Sec > 59) { MessageBox.Show("Секунд не может быть больше 59", "Ошибка ввода времени", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }
                sailingTime2c.Min = Convert.ToUInt32(textBox5.Text);
                sailingTime2c.Sec = Convert.ToUInt32(textBox6.Text);
                sailingTimeAustera.Min = Convert.ToUInt32(textBox7.Text);
                sailingTimeAustera.Sec = Convert.ToUInt32(textBox8.Text);
            }
            catch { MessageBox.Show("Тут должны быть циферки!", "Ошибка ввода времени", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }
            allSave();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            try
            {
                currentTime.Min = Convert.ToUInt32(textBox1.Text); //if (currentTime.Min > 20) { MessageBox.Show("Минут не может быть больше 19 у кораблика", "Ошибка ввода времени", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }
                currentTime.Sec = Convert.ToUInt32(textBox2.Text); //if (currentTime.Sec > 59) { MessageBox.Show("Секунд не может быть больше 59", "Ошибка ввода времени", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }
            }
            catch { MessageBox.Show("Тут должны быть циферки!", "Ошибка ввода времени", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }
            if (comboBox1.SelectedIndex == 0) { nowStatus = statuses[2]; } else if (comboBox1.SelectedIndex == 1) { nowStatus = statuses[0]; } else { MessageBox.Show("Выберите где сейчас стоит кораблик", "Ошибка выбора локации", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }
            timer1.Enabled = true;
            changePicture();
            allSave();
        }
    }
}
