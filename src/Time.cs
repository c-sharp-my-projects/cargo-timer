﻿namespace CargoTimer
{
    public class Time
    {
        public Time(string str)
        {
            //if (str == "stay") { min = 20; sec = 00; } else if (str == "sailingTo2c") { min = 10; sec = 20; } else if (str == "sailingToAustera") { min = 10; sec = 47; }
        }
        public Time() { }

        public uint TotalSeconds
        {
            get
            {
                return min*60+sec;
            }
        }
        private uint min;
        public uint Min
            {
                get
                {
                    return min;
                }

                set
                {
                    min = value;
                }
            }
        private uint sec;
        public uint Sec
            {
                get
                {
                    return sec;
                }

                set
                {
                    sec = value;
                }

        }
    }
}
